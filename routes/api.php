<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::post('/register',  [\App\Http\Controllers\Api\V1\AuthController::class, 'registerAction']);
    Route::post('/login',  [\App\Http\Controllers\Api\V1\AuthController::class, 'loginAction']);
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::post('/auth/status',  [\App\Http\Controllers\Api\V1\AuthController::class, 'authStatusAction']);
        Route::get('/user',  [\App\Http\Controllers\Api\V1\AuthController::class, 'userAction']);
        Route::post('/user',  [\App\Http\Controllers\Api\V1\AuthController::class, 'updateUserAction']);
    });
        Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
            return $request->user();
        });
});


// Following route must be last
Route::any('/{api_routes?}', function () {
    return response()->json(['message' => 'Not Found!'], 404);
})->where('api_routes', '[\/\w\.-]*');
