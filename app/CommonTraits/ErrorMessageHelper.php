<?php

/**
 * Created by PhpStorm.
 * User: Tariq Mahmood
 * Date: 07/10/21
 * Time: 02:35 AM
 */

namespace App\CommonTraits;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\Lang;

/**
 * Trait ErrorMessageHelper
 * @package App\CommonTraits
 */
trait ErrorMessageHelper
{

    /**
     * Get the forbidden message.
     *
     * @return array
     */
    protected function getBooleanResponse($status)
    {
        return array('status' => $status);
    }


    /**
     * Get the fail Login message.
     *
     * @return array
     */
    protected function getAuthFailMessage()
    {
        return array(
            'status' => 401,
            'data' => array(
                'type' => Lang::has('errors.type.unauthenticated') ? Lang::get('errors.type.unauthenticated') : 'unauthenticated',
                'description' => Lang::has('errors.description.unauthenticated') ? Lang::get('errors.description.unauthenticated') : 'Unauthenticated.'
            )
        );
    }

    /**
     * Get the forbidden message.
     *
     * @return array
     */
    protected function getForbiddenMessage()
    {
        return array(
            'status' => 403,
            'data' => array(
                'type' => Lang::has('errors.type.forbidden') ? Lang::get('errors.type.forbidden') : 'forbidden',
                'description' => Lang::has('errors.description.forbidden') ? Lang::get('errors.description.forbidden') : 'Access forbidden. The request may not be properly authorized.'
            )
        );
    }



    /**
     * Get the not found message.
     *
     * @return array
     */
    protected function getNotFoundMessage()
    {
        return array(
            'status' => 404,
            'data' => array(
                'type' => Lang::has('errors.type.notfound') ? Lang::get('errors.type.notfound') : 'notFound',
                'description' => Lang::has('errors.description.notfound') ? Lang::get('errors.description.notfound') : 'Not found.'
            )
        );
    }
    /**
     * Get the invalid parameter message.
     *
     * @return array
     */
    protected function getValidationFailMessage($validationError)
    {
        return array(
            'status' => 422,
            'data' => array(
                'type' => Lang::has('errors.type.invalid_parameter') ? Lang::get('errors.type.invalid_parameter') : 'invalid_parameter',
                'description' => $validationError
            )
        );
    }

    /**
     * @param string $type
     * @return array
     */
    protected function getInvalidParameterMessage()
    {

        return array(
            'status' => 400,
            'data' => array(
                'type' => Lang::has('errors.type.invalid_parameter') ? Lang::get('errors.type.invalid_parameter') : 'invalid_parameter',
                'description' => Lang::has('errors.description.invalid_parameter') ? Lang::get('errors.description.invalid_parameter') : 'A required parameter is missing from the request.'
            )
        );
    }



    /**
     * Get the fail Login message.
     *
     * @return array
     */
    protected function getFailedLoginMessage()
    {
        return array(
            'status' => 401,
            'data' => array(
                'type' => Lang::has('errors.type.fail_login') ? Lang::get('errors.type.fail_login') : 'wrong attempt to Login',
                'description' => Lang::has('errors.description.fail_login') ? Lang::get('errors.description.fail_login') : 'Invalid username or password.'
            )
        );
    }

    /**
     * @param $code
     * @param $message
     * @return array
     */
    protected function getExceptionMessage($code, $description)
    {
        if ($code < 1) {
            $code = 500;
        }
        $type = Lang::has('errors.type.exception') ? Lang::get('errors.type.exception') : 'Exception';
        if(env('APP_DEBUG') === false AND env('APP_LOG_LEVEL') === 'production'){
            $code = 501;
            $description = Lang::has('errors.description.exception') ? Lang::get('errors.description.exception') : 'Internal Server Error!';
        }
        return array(
            'status' => $code,
            'data' => array(
                'type' => $type,
                'description' => $description
            )
        );
    }



}
