<?php

/**
 * Created by PhpStorm.
 * User: Tariq Mahmood
 * Date: 07/07/21
 * Time: 02:20 AM
 */

namespace App\CommonTraits;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Lang;

/**
 * Trait MainResponse
 * @package App\CommonTraits
 */
trait MainResponse
{
    use GenericResponse;

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show($id)
    {
        return $this->customReturnView($this->repository->show($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit($id)
    {
        return $this->customReturnView($this->repository->edit($id));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id)
    {
        return $this->customReturnView($this->repository->update($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        $result = $this->repository->destroy($id);
        return $this->returnView($result);
    }
}
