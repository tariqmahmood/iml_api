<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GeneralEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $subject;
    public $template;
    public $locale;

    /**
     * GeneralEmail constructor.
     * @param array $data
     * @param string $template
     * @param string $subject
     * @param string $locale
     */
    public function __construct($data, $template, $subject = 'ILM Mail!', $locale = 'en-us')
    {
        $this->data = $data;
        $this->subject = $subject;
        $this->template = $template;
        $this->locale = $locale;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
            ->view($this->template)
            ->locale($this->locale);

    }
}
