<?php

/**
 * Created by PhpStorm.
 * User: Tariq Mahmood
 * Date: 07/10/21
 * Time: 02:45 AM
 */

namespace App\Repositories\Api\V1;

use App\Mail\GeneralEmail;
use App\Models\User;
use DB;
use Illuminate\Support\Str;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;

/**
 * Class AuthRepository
 * @package App\Repositories\Api\V1
 */
class AuthRepository extends AbstractRepository
{
    /**
     *
     * @param Request $request
     * @param  \App\Models\User $model
     */
    public function __construct(User $model, Request $request)
    {
        parent::__construct($model, $request);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getProfile(Request $request)
    {
        return  $request->user();
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'access_token' => 'required|exists:users,access_token',
        ]);
        if ($validator->fails()) {
            return $this->getValidationFailMessage($validator->errors()->all());
        }
        $user = User::where('access_token', $request->get('access_token'))
            ->first();
        if (!$user) {
            return $this->getFailedLoginMessage();
        }

        $token = $user->createToken($user->email);
        $user->access_token = null;
        $user->save();
        return [
            'user' => $user,
            'api_url' => env('APP_URL', 'http://127.0.0.1:8000'),
            'token' => $token->plainTextToken,
        ];
    }


    /**
     * @param Request $request
     * @return array|mixed
     */
    public function register(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|string|email|max:255',
            ]);
            if ($validator->fails()) {
                return $this->getInvalidParameterMessage();
            }
            $user = User::where('email', $request->email)->first();
            if (!$user) {
                $user = $this->createUser($request->all());
            }else{
                $user->access_token = $this->generateToken(25);
                $user->save();
            }
            $access_url  = env('APP_URL', 'https'). '/login/' .$user->access_token;

            Mail::to($user->email)
                ->send(new GeneralEmail(['name' => $user->name, 'email' => $user->email, 'email_token' => $user->access_token, 'access_url' => $access_url], 'emails.verification', 'Verify Your Account - ILM'));
            return $this->getBooleanResponse(204);
        } catch (\Exception $e) {
            return $this->getExceptionMessage(401, $e->getMessage());
        }
    }


    /**
     * @param Request $request
     * @return array|mixed
     */
    public function authStatus(Request $request)
    {
        return $this->getBooleanResponse(204);

    }

    /**
     * @param Request $request
     * @return array|mixed
     */
    public function userInfo(Request $request)
    {
        return $request->user();
    }
    /**
     * @param Request $request
     * @return array|mixed
     */
    public function updateUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'age' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return $this->getInvalidParameterMessage();
        }
        $user =  $request->user();
        $user->name = $request->name;
        $user->age = $request->age;
        $user->is_profile_updated = true;
        $user->save();
        return $user;
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function createUser(array $data)
    {

        $user = User::create([
            'email'     => $data['email'],
            'password'  => bcrypt('ilm_TT_psw'),
            'access_token'  => $this->generateToken(25)
        ]);

        return $user;
    }



    /**
     * @param int $length
     * @return string
     */
    protected function generateToken($length = 60)
    {
        $token = Str::random($length);;
        if ($this->tokenExists($token)) {
            return $this->generateToken($length);
        }

        return $token;
    }

    /**
     * @param $token
     * @return bool
     */
    protected function tokenExists($token)
    {
        return User::whereAccessToken($token)->exists();
    }


}
