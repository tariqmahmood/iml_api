<?php

/**
 * Created by PhpStorm.
 * User: Tariq Mahmood
 * Date: 07/10/21
 * Time: 03:15 AM
 */

namespace App\Repositories\Api\V1;

use App\CommonTraits\ErrorMessageHelper;
use App\Contracts\Repositories\Api\V1\Portal\CrudRepository as Contract;
use App\Events\GeneralBroadcast;
use App\Mail\GeneralEmail;
use App\Models\Audit;
use App\Models\Discussion;
use App\Models\Folder;
use App\Models\FolderScaffold;
use App\Models\ModelView;
use App\Models\Note;
use App\Models\Notification;
use App\Models\Statistic;
use App\Models\Upload;
use App\Models\User;
use App\Models\UserSetting;
use App\Repositories\Api\V1\Portal\NotificationRepository;
use App\Repositories\ChartRepository;
use App\Repositories\RepositoryHelper;
use Carbon\Carbon;
use DB;
//use Doctrine\Common\Inflector\Inflector;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Mail;
use Validator;

/**
 * Class AbstractRepository
 * @package App\Repositories\Api\V1
 */
abstract class AbstractRepository
{

    use ErrorMessageHelper;

    /**
     * @var int
     */
    protected $maxPerPage = 50;

    /**
     * @var int
     */
    protected $currentPage = 1;

    /**
     * The model to execute queries on.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * The model to execute queries on.
     *
     * @var Request
     */
    protected $request;

    /**
     * @var array
     */
    protected $generic_except = ['joinWith'];

    /**
     * AbstractRepository constructor.
     * @param Request $request
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function __construct(Model $model, Request $request)
    {
        $this->model = $model;
        $this->request = $request;
        $this->paginateOption($request);
    }




    /**
     * @return array|mixed
     */
    public function create()
    {


        try {


            DB::beginTransaction();

            $model = $this->model::create($this->request->except($this->generic_except));
            $model = $model->refresh();
            DB::commit();

            return $this->edit($model->id, $this->request->joinWith);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return $this->getExceptionMessage(503, $e->getMessage());
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->getExceptionMessage(503, $e->getMessage());
        }

    }


    /**
     * @param $ids
     * @param $model
     * @return array
     */
    protected function deleteMultiple($ids, $model)
    {
        try {
            $table_name = $model->getTable();
            $ids = json_decode($ids);
            $model::find($ids)->each(function ($key, $val) {
                $key->delete();
            });
            return $this->getBooleanResponse(204);
        } catch (\Illuminate\Database\QueryException $e) {
            return $this->getExceptionMessage($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->getExceptionMessage($e->getCode(), $e->getMessage());
        }
    }

    /**
     * @param $id
     * @return array|mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        try {
            if ($this->request->multiple) {
                return $this->deleteMultiple($this->request->multiple, $this->model);
            } else {

                $table_name = '';

                if ($this->request->has('force_delete') && $this->request->force_delete) {
                    $modelObj = $this->model::withTrashed()->find($id);
                    if (!$modelObj) {
                        return $this->getNotFoundMessage();
                    }
                    $this->createAudit($modelObj, 'delete');
                    $table_name = $modelObj->getTable();
                    $modelObj->forceDelete();
                } else {
                    $modelObj = $this->model::find($id);
                    if (!$modelObj) {
                        return $this->getNotFoundMessage();
                    }
                    $this->createAudit($modelObj, 'delete');
                    // $this->auditAction($modelObj, 'deleted_by');
                    $table_name = $modelObj->getTable();
                    $modelObj->delete();
                }
                $this->emitModelUpdate(['model' => $table_name]);
                return $this->getBooleanResponse(204);
            }

        } catch (QueryException $e) {
            return $this->getExceptionMessage($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->getExceptionMessage($e->getCode(), $e->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public function index()
    {
        try {

            $query = $this->model;

            $query = $this->addWhere($query);
            $query = $this->searchByAll($query, $this->model);
            $query = $this->addJoin($query);

            $query = $this->orderByAction($query);
            return $this->resultType($query);
        } catch (\Illuminate\Database\QueryException $e) {
            return $this->getExceptionMessage($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->getExceptionMessage($e->getCode(), $e->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public function resultType($query)
    {
        if ($this->request->has('page')) {
            return $query->paginate($this->maxPerPage)->appends($this->request->all());
        } elseif ($this->request->has('limit')) {
            if ($this->request->has('order_type') && $this->request->order_type == 'desc') {
                $id_order_condition = '<';
            } else {
                $id_order_condition = '>';
            }
            if ($this->request->has('id')) {
                $query = $query->where('id', $id_order_condition, $this->request->id);
            }
            return $query->take($this->maxPerPage)->get();
        } else {
            return $query->get();
        }
    }


    /**
     * @param $query
     * @param array $filtr
     * @return mixed
     */
    protected function addWhere($query, $filtr = array())
    {

        $filters = array();
        if ($this->request->has('filterArray') && is_array(json_decode($this->request->filterArray, true))) {
            $filters = json_decode($this->request->filterArray, true);
        }

        if (is_array($filtr) && $filtr != array()) {
            $filters[] = $filtr;
        }

        if ($this->request->has('filterBy') && $this->request->has('filterValue')) {
            $filter = [
                'column' => $this->request->filterBy,
                'value' => $this->request->filterValue,
            ];

            if ($this->request->has('filterCondition')) {
                $filter['condition'] = $this->request->filterCondition;
            }
            if ($this->request->has('filterMethod')) {
                $filter['method'] = $this->request->filterMethod;
            }
            $filters[] = $filter;
        }

        return $this->filterByArray($query, $filters);
    }

    /**
     * @param $query
     * @param $filters
     * @return mixed
     */
    protected function searchByAll($query, $model)
    {

        if ($this->request->has('filter')) {
            $searchFields = array();
            $filter = $this->request->filter;
            $check_filter_join_fields = true;  // if model search field apply then it only check wihin that block
            $joinFilterType = 'whereHas'; // this apply when join_filter_fields param passed
            if ($this->request->has('filterFields') && $this->request->filterFields != 'all') {

                $searchFields = explode(',', $this->request->filterFields);
            } else {
                if($this->request->has('filter_join')){
                    $table = $this->request->filter_join;
                    $joinName = str_singular($table);
                    if($this->request->has('filter_join_type') AND $this->request->filter_join =='many'){
                        $joinName = $table;
                    }
                }else{
                    $table = $model->getTable();
                }
                $columns = DB::select("SHOW FULL COLUMNS FROM $table");
                $i = 0;
                $typeErrors = [];
                foreach ($columns as $column) {
                    $excludeTypes = array(
                        'datetime',
                        'timestamp',
                        'time',
                        'date',
                        'int',
                        'int unsigned',
                        'int(10) unsigned',
                        'int(11)',
                        'tinyint(1)'
                    );
                    if (!in_array($column->Type, $excludeTypes)) {
                        if($table == 'users' and ($column->Field ==='password')) $typeErrors[] = $column->Type;
                        else $searchFields[] = $column->Field;

                    }
                    $i++;
                }
            }


            // Note: filter join cannot be pass with filterFields param if pass only filterFields has there priority
            if($this->request->has('filter_join') and !$this->request->has('filterFields')){
                $joinFilterType = 'orWhereHas';
                $query = $query->with($joinName)->whereHas($joinName, function ($qry) use ($searchFields, $filter){
                    $i = 0;
                    foreach ($searchFields as $column) {
                        if ($i === 0) {
                            $qry->where(trim($column), 'like', '%' . $filter . '%');
                        } else {
                            $qry->orWhere(trim($column), 'like', '%' . $filter . '%');
                        }
                        $i++;
                    }
                });

            }else{
                $joinFilterType = 'orWhereHas';
                $check_filter_join_fields = false;
                $query = $query->where(function ($qry) use ($searchFields, $filter, $joinFilterType) {

                    $i = 0;
                    $apply_numeric_srch = true;
                    if($this->request->has('request_from') and $this->request->request_from === 'dropdown'){
                        $apply_numeric_srch = false;
                    }
                    if(is_numeric($filter) and $apply_numeric_srch){
                        $searchFields[] = 'id';
                    }
                    foreach ($searchFields as $column) {
                        if ($i === 0) {
                            $qry->where(trim($column), 'like', '%' . $filter . '%');
                        } else {
                            $qry->orWhere(trim($column), 'like', '%' . $filter . '%');
                        }
                        $i++;
                    }
                    if ($this->request->has('filter_join_fields')) {
                        $filter_relations = str_replace(' ', '', $this->request->filter_join_fields);
                        $relations = explode(',', $filter_relations);
                        foreach ($relations as $relation) {

                            $joins = explode('~', $relation);
                            if (count($joins) > 0) {
                                $join = $joins[0];
                                $joinFieldsString = $joins[1];
                                $join_fields = explode('-', $joinFieldsString);

                                $query = $qry->$joinFilterType($join, function ($qry) use ($filter, $join_fields){
                                    $i = 0;
                                    foreach ($join_fields as $column) {
                                        if ($i === 0) {
                                            $qry->where(trim($column), 'like', '%' . $filter . '%');
                                        } else {
                                            $qry->orWhere(trim($column), 'like', '%' . $filter . '%');
                                        }
                                        $i++;
                                    }
                                });
                            }
                        }
                    }
                });
            }
            if ($this->request->has('filter_join_fields') && $check_filter_join_fields) {
                $filter_relations = str_replace(' ', '', $this->request->filter_join_fields);
                $relations = explode(',', $filter_relations);
                foreach ($relations as $relation) {

                    $joins = explode('~', $relation);
                    if (count($joins) > 0) {
                        $join = $joins[0];
                        $joinFieldsString = $joins[1];
                        $join_fields = explode('-', $joinFieldsString);

                        $query = $query->$joinFilterType($join, function ($qry) use ($filter, $join_fields){
                            $i = 0;
                            foreach ($join_fields as $column) {
                                if ($i === 0) {
                                    $qry->where(trim($column), 'like', '%' . $filter . '%');
                                } else {
                                    $qry->orWhere(trim($column), 'like', '%' . $filter . '%');
                                }
                                $i++;
                            }
                        });
                    }
                }
            }
        }




        return $query;
    }

    /**
     * @param $query
     * @param $filters
     * @return mixed
     */
    protected function filterByArray($query, $filters)
    {

        if (!is_array($filters)) {
            return $query;
        }
        if (is_array($filters) && $filters == array()) {
            return $query;
        }
        if(count($filters) > 0){
            $query = $query->where(function ($query) use ($filters) {
                foreach ($filters as $filter) {

                    $column_name = $filter['column'];
                    $method = 'where';
                    $filter_condition = '';
                    $methodSuffix = '';
                    $is_null_condition = false;

                    $filter_value = $filter['value'];

                    if (isset($filter['method']) && (strtolower($filter['method']) == 'orwhere' or strtolower($filter['method']) == 'or_where')) {
                        $method = 'orWhere';
                    }

                    if (isset($filter['condition']) && (strtolower($filter['condition']) == 'between' or strtolower($filter['condition']) == 'notbetween' or $filter['condition'] == 'in' or $filter['condition'] == 'notin')) {

                        $filter_condition = '';
                        $methodSuffix = ucfirst(Str::camel($filter['condition']));
                        if ($filter['condition'] && is_array(json_decode($filter['value'], true))) {
                            $filter_value = json_decode($filter['condition'], true);
                        } else {
                            $inValue = str_replace(' ', '', $filter['value']);
                            $filter_value = explode(',', $inValue);
                        }
                    } elseif (isset($filter['condition']) && (strtolower($filter['condition']) == 'null' or strtolower($filter['condition']) == 'notnull')) {

                        $methodSuffix = ucfirst(Str::camel($filter['condition']));
                        $filter_condition = strtolower($filter['condition']);
                        $filter_value = '';
                        $is_null_condition = true;
                    } elseif (isset($filter['condition']) && (strtolower($filter['condition']) == 'like')) {

                        $methodSuffix = '';
                        $filter_condition = strtolower($filter['condition']);
                        $filter_value = '%' . $filter['value'] . '%';
                    } elseif (isset($filter['condition'])) {
                        $filter_condition = $filter['condition'];
                    }
                    if (isset($filter['date'])) {

                        if (strtolower($filter['date']) == 'datebetween') {
                            $dateValue = explode(',', $filter['value']);
                            $Value = array_map('trim', $dateValue);
                            $methodSuffix = 'Between';
                            $filter_condition = '';
                            $filter_value = $Value;
                        } elseif (strtolower($filter['date']) == 'datetime') {
                            $Value = explode(',', $filter['value']);
                            $methodSuffix = '';
                            $filter_condition = $filter['condition'];
                            $filter_value = $Value;
                        } else {

                            $methodSuffix = ucfirst(Str::camel($filter['date']));
                            $filter_condition = $filter['condition'];
                            $filter_value = $filter['value'];
                        }
                    }

                    $filter_method_name = sprintf($method . '%s', $methodSuffix);

                    if (!empty($filter_condition)) {
                        if ($is_null_condition) {
                            $query = $query->$filter_method_name(trim($column_name));
                        } else {
                            $query = $query->$filter_method_name(trim($column_name), trim($filter_condition), $filter_value);
                        }
                    } else {

                        $query = $query->$filter_method_name(trim($column_name), $filter_value);
                    }
                }
            });

        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    protected function addJoin($query)
    {

        if ($this->request->has('trashed') && $this->request->trashed == true) {
            $query = $query->withTrashed();
        } elseif ($this->request->has('onlyTrashed') && $this->request->onlyTrashed == true) {
            $query = $query->onlyTrashed();
        }

        if ($this->request->has('joinWith')) {

            $relations = str_replace(' ', '', $this->request->joinWith);
            $relations = explode(',', $relations);

            foreach ($relations as $relation) {

                $joins = explode('~', $relation);
                if (count($joins) > 1) {
                    $join = $joins[0];
                    $filterString = $joins[1];
                    $filters = explode('-', $filterString);
                    $query = $query->with([
                        $join => function ($query) use ($filters) {
                            foreach ($filters as $filter) {
                                $filterArray = explode(':', $filter);
                                $query->where($filterArray[0], $filterArray[1]);
                            }
                        }
                    ]);
                } else {
                    $join = $joins[0];
                    $query = $query->with($join);
                }
            }
        }
        if($this->request->has('have_not_join')){
            $query = $query->doesntHave($this->request->have_not_join);
        }
        if($this->request->has('has_join')){
            $query = $query->has($this->request->has_join);
        }

        return $query;
    }


    /**
     * @param $query
     * @return mixed
     */
    protected function orderByAction($query)
    {


        $order_by = 'id';
        $order_type = 'asc';

        if ($this->request->has('filterOrder') && is_array(json_decode($this->request->filterOrder, true))) {
            $orders = json_decode($this->request->filterOrder, true);
            foreach ($orders as $order) {

                $query = $query->orderBy(trim($order['column']), trim($order['order']));
            }
        } elseif ($this->request->has('order_by') || $this->request->has('order_type')) {


            if ($this->request->has('order_by')) {
                $order_by = $this->request->order_by;
            }
            if ($this->request->has('order_type') && $this->request->order_type == 'desc') {
                $order_type = 'desc';
            }

            $query = $query->orderBy($order_by, $order_type);
        }

        return $query;
    }

    /**
     * @param Request $request
     */
    private function paginateOption(Request $request)
    {

        if ($request->has('page') and $request->get('page') > 0) {
            $this->currentPage = $request->get('page');
        }

        if ($request->has('limit') and $request->get('limit') == 'all') {
            $this->maxPerPage = 500;
        } elseif ($request->has('limit') and $request->get('limit') > 0) {
            $this->maxPerPage = $request->get('limit');
        }

        $page = $this->currentPage;

        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
    }

    /**
     * @param $id
     * @return array
     */
    protected function restore($id)
    {

        $modelObj = $this->model::onlyTrashed()->find($id);
        if (!$modelObj) {

            return $this->getNotFoundMessage();
        }

        $modelObj->restore();
        return $modelObj;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->edit($id, $this->request->joinWith);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id, $joinWith = null)
    {
        try {
            $qryBuildFrom = $this->model::withTrashed();
            $firstQry = clone $qryBuildFrom;
            $first = $firstQry
                ->oldest()
                ->orderBy('id', 'asc')
                ->first();
            $first_id = null;
            if ($first) {
                $first_id = $first->id;
            }

            $lastQry = clone $qryBuildFrom;

            $last = $lastQry
                ->latest()
                ->orderBy('id', 'desc')
                ->first();
            $last_id = null;
            if ($last) {
                $last_id = $last->id;
            }
            $currentQry = clone $qryBuildFrom;
            $current_page = $currentQry
                ->where('id', '<', $id)
                ->count();

            $current_id = null;
            $query = clone $qryBuildFrom;
            if ($joinWith) {

                $relations = str_replace(' ', '', $joinWith);
                $relations = explode(',', $relations);

                foreach ($relations as $relation) {
                    $query = $query->with($relation);
                }
            }
            $query = $this->addJoin($query);
            $result = $query->find($id);
            if ($result) {
                $current_page++;
                $current_id = $result->id;
                $result->counts = $this->getChildModelCounts($result);
            }

            $previous_id = null;

            $previousQry = clone $qryBuildFrom;
            $previous = $previousQry
                ->where('id', '<', $id)
                ->orderBy('id', 'desc')
                ->first();
            if ($previous) {
                $previous_id = $previous->id;
            }
            $next_id = null;
            $nextQry = clone $qryBuildFrom;
            $next = $nextQry
                ->where('id', '>', $id)
                ->orderBy('id', 'asc')
                ->first();
            if ($next) {
                $next_id = $next->id;
            }

            $totalQry = clone $qryBuildFrom;
            $total = $totalQry
                ->count();

            return [
                'paging' => [
                    'first' => $first_id,
                    'previous' => $previous_id,
                    'current' => $current_id,
                    'next' => $next_id,
                    'last' => $last_id,
                    'current_page' => $current_page,
                    'total' => $total
                ],
                'data' => $result
            ];
        } catch (\Exception $e) {

            return $this->getExceptionMessage(422, $e->getMessage());
        }
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function update($id)
    {
        if ($this->request->has('restore')) {
            return $this->restore($id);
        }
        $modelObj = $this->model::find($id);
        if (!$modelObj) {

            return $this->getNotFoundMessage();
        }

        try {

            $modelObj->update($this->request->except($this->generic_except));

            return $this->edit($modelObj->id, $this->request->joinWith);
        } catch (\Illuminate\Database\QueryException $e) {
            return $this->getExceptionMessage($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->getExceptionMessage($e->getCode(), $e->getMessage());
        }
    }

}
