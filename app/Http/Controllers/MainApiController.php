<?php

/**
 * Created by PhpStorm.
 * User: Tariq Mahmood
 * Date: 07/10/21
 * Time: 02:12 AM
 */

namespace App\Http\Controllers;

use App\CommonTraits\MainResponse;
use App\CommonTraits\ErrorMessageHelper;

abstract class MainApiController extends Controller
{
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_NO_CONTENT = 204;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_INTERNAL_SERVER_ERROR = 500;

    use MainResponse, ErrorMessageHelper;
}
