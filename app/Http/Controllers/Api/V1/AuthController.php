<?php

/**
 * Created by PhpStorm.
 * User: Tariq Mahmood
 * Date: 01/10/21
 * Time: 02:39 AM
 */

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\MainApiController;
use App\Repositories\Api\V1\AuthRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends MainApiController
{
    /*
    |
    |--------------------------------------------------------------------------
    | Auth Controller
    |--------------------------------------------------------------------------
    |
    */

   // use AuthenticatesUsers;

    /**
     * The user repository instance.
     *
     * @var \App\Repositories\Api\V1\AuthRepository
     */
    protected $auth;

    /**
     * AuthController constructor.
     * @param AuthRepository $auth
     */
    public function __construct(AuthRepository $auth)
    {
        $this->auth = $auth;
        //  $this->middleware('guest', ['except' => 'logout']);
    }



    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        $result = $this->auth->login($request);
        return $this->returnView($result);
    }



    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $result = $this->auth->register($request);
        return $this->returnView($result);
    }


    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function authStatusAction(Request $request)
    {
        $result = $this->auth->authStatus($request);
        return $this->returnView($result);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userAction(Request $request)
    {
        $result = $this->auth->userInfo($request);
        return $this->returnView($result);
    }
    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateUserAction(Request $request)
    {
        $result = $this->auth->updateUser($request);
        return $this->returnView($result);
    }
}
