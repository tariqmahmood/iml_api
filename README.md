# Test Project #

Tariq Mahmood

### What is this repository for? ###

* This README is designed to add
* laravel Api project 

### How do I get set up? ###

**Clone** ILM Api Repository

```bash
 $ git clone https://tariqmahmood@bitbucket.org/tariqmahmood/iml_api.git
```


### Install laravel Dependencies 
```bash
 $ composer install 
 # this command install laravel dependencies and create env file (copy of .env.example
 # After Composer Install Update .env file according to your setting setting 
 
 # DB_DATABASE=iml_api
 # DB_USERNAME=root
 # DB_PASSWORD=rootroot
 
 # Create New Database in MySQl name= env(DB_DATABASE) i.e iml_api
```


### Run Migrations 
```bash
 # after composer install and database create please run 
 $ php artisan migrate

 # To refresh existing database 
 $ php artisan migrate:refresh

```

### Run laravel Dev Server  
```bash
 $ php artisen serve 
 # Open in Boswer : http://127.0.0.1:8000 
```

### Production Build Note 
```bash
 # Currently i add production Build in laravel as well when you open  http://127.0.0.1:8000  it will show front app production build there 
 # Frontend of this application is create via Quasar Framework :
 # Clone Frontend repo: git clone https://tariqmahmood@bitbucket.org/tariqmahmood/iml_frontend.git
```


### Start Email Queue
```bash
 # Send email by laravel Queue System  @if **QUEUE_DRIVER=database** in .env
 $ php artisan queue:work --tries=1
 # I use Sendgrid to send email 
```
## Developer Detail
### Tariq Mahmood 
### tariqmmahmood@gmail.com
### +923457572525
### skype: tariqmmahmood

